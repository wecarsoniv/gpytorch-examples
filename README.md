# Gaussian Process Regression with GPyTorch


## Introduction

This directory contains materials and examples on how to use the GPyTorch library for quick and efficient Gaussian process inference. Examples were made as part of a class project for ECE 590 - Advanced Topics in Deep Learning.


## Technologies

This project and examples rely on the following libraries and technologies:

* Python >= 3.8
* [PyTorch](https://pytorch.org/) >= 1.8.0
* [GPyTorch](https://gpytorch.ai/) >= 1.4.0
* [scikit-learn](https://scikit-learn.org/stable/) 0.24.1

Key libraries and version numbers are listed. Other libraries are omitted for brevity.


## **Creating the GPyTorch Environment in Anaconda**

Provided in this directory is a .yml file with common Python packages and the dependencies necessary to run GPyTorch. Run the following command in a terminal or Anaconda Prompt to create a conda environment from the .yml file:

```bash
conda env create -f gpytorch_env.yml
```

To activate the environment, run the following command:

```bash
conda activate myenv
```

## **GPyTorch Examples**

We also provide a examples on how to use GPyTorch in the `gpytorch_regression_examples.ipynb` notebook. For additional resources or examples, GPyTorch provides extensive documentation on their [website](https://docs.gpytorch.ai/en/latest/index.html).

